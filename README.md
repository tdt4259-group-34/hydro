# Hydro Challenge: Predicting Product Mix Based on History

_TDT4259 Applied Data Science @ NTNU, Group 34_

* `/datasets` - contains the raw and preprocessed datasets.
* `/profiling_reports` - contains the pandas profiling reports for both the raw and preprocessed datasets.
* `/tests` - contains unused files.
* `data_preprocessing.ipynb` - preprocessing of the raw dataset.
* `decision_tree.ipynb` - implementation of a decision tree regression model for predicting product fractions.
* `lstm.ipynb` - implementation of a LSTM model for predicting product fractions.
